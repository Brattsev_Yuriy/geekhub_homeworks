package com.company;

import java.util.Scanner;

public class Converter {
    private byte number;
    private String word;
    private String[] words = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};

    public void setNumber() {
        System.out.println("Set number: ");
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextByte();
    }

    public String getWord() {
        setNumber();
        word = words[number];
        return word;
    }
}

