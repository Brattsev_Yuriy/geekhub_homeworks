package com.company;

public class Fibonacci {

    public void generate() {
        int a = 1;
        int b = 1;

        for (int i = 0; i < 10; i++) {
            System.out.print(a + " " + b + " ");
            a += b;
            b += a;
        }
    }
}

