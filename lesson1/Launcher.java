package com.company;

import java.util.Scanner;

public class Launcher {

    public static void main(String[] args) {
        byte choice;
        System.out.println("1. Count factorial of number");
        System.out.println("2. Generate Fibonacci");
        System.out.println("3. Convert number into word");
        Scanner scanner = new Scanner(System.in);
        choice = scanner.nextByte();
        switch (choice) {
            case 1:
                Factorial factorial = new Factorial();
                System.out.println(factorial.getResult());
                break;
            case 2:
                Fibonacci fibonacci = new Fibonacci();
                fibonacci.generate();
                break;
            case 3:
                Converter converter = new Converter();
                System.out.println(converter.getWord());
                break;
            default:
                System.out.println("Wrong number");
                break;

        }
    }
}

