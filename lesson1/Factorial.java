package com.company;

import java.util.Scanner;

public class Factorial {
    private byte number;
    private int result = 1;

    public void setNumber() {
        System.out.println("Set number: ");
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextByte();
    }

    public void count() {
        setNumber();
        for (int i = 1; i < number + 1; i++) {
            result *= i;
        }
    }

    public int getResult() {
        count();
        return result;
    }
}

